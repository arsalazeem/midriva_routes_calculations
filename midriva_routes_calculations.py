import pdb
# import time
import datetime
# import baseurl
import requests
import haversine as hs
from get_points_from_api import return_rideRouteTracking
import json
import traceback

config={
    "live_route":False,
    "debugging":False
}

def break_point() -> None:
    debugging = False
    if debugging:
        pdb.set_trace()
    else:
        pass


def get_routes(key=None):
    if key is None:
        try:
            print("Please provide ride id")
            ride_id = input()
            rideRouteTrackinglocal = return_rideRouteTracking(ride_id)
            return rideRouteTrackinglocal
        except Exception as e:
            print(e)
            print("Error in getting points from job object.Please provide a valid Job id.")
            pdb.set_trace()
            exit()




def generate_route_url():
    # sorted_list()
    custom_list = []
    url = "https://www.google.com/maps/dir"
    total = ""
    rideRouteTracking = sorted_list("driverCurrentTi"
                                    ""
                                    ""
                                    ""
                                    "me")
    for i in rideRouteTracking:
        # print(giveme_time(i.get("receivingTime")))
        # time.sleep(10)
        # print(i.get("sequenceNumber"))
        total = total + ("'/'" + i.get("latitude") + "," + i.get("longitude"))
    total = total[1:]
    url = url + total
    print(url)
    if config.get("live_route"):
        driver = init_chrome.start()
        driver.get(url)
        break_point()
    else:
        pass


def give_difference(current, next):
    difference = next - current
    return difference


def giveme_time(unix_time):
    timestamp = datetime.datetime.fromtimestamp(unix_time)
    return timestamp.strftime('%Y-%m-%d %H:%M:%S')


def get_average_speed():
    total = 0
    rideRouteTracking = sorted_list("driverCurrentTime")
    for i in rideRouteTracking:
        total = total + i.get("speed")
    # print(total)


def get_slow_moving_total_time_in_seconds(speed):
    slow_moving_list = []
    total = 0
    rideRouteTracking = sorted_list("driverCurrentTime")
    for i in rideRouteTracking:
        if i.get("speed") <= speed:
            a = i.get("time")
            a = float("{:.2f}".format(a))
            slow_moving_list.append(a)
            total = total + a
    print("SLow moving time for this ride in minutes=" + str(total))
    # print(slow_moving_list)


def get_slow_moving_total_time_in_minutes(speed):
    total = 0
    rideRouteTracking = sorted_list("driverCurrentTime")
    for i in rideRouteTracking:
        if i.get("speed") <= speed:
            a = i.get("time")
            a = float("{:.2f}".format(a))
            total = total + a
    # print("SLow moving time for this ride in minutes="+str(total/60))


def show_time_for_all_points():
    rideRouteTracking = sorted_list("driverCurrentTime")
    for i in rideRouteTracking:
        print(i.get("time"))


def create_custom_lenght_routes(lenght):
    rideRouteTracking = sorted_list("driverCurrentTime")
    temp_list = []
    k = 0
    for i in rideRouteTracking:
        k = k + 1
        temp_list.append(i)
        print(k)
        if k > lenght:
            return temp_list
        else:
            pass


def _calculate_distance():
    sorted_list()
    session = requests.Session()
    login = {
        "email": "admin@blink.com",
        "password": "admin1234"
    }

    response = session.post("https://stagingapi.eblink.io/api/v1/en/admin/logIn", data=login)
    custom_object = {"data": create_custom_lenght_routes(2)}
    # print(custom_object)
    # pdb.set_trace()
    response = session.post("https://stagingapi.eblink.io/api/v1/en/calculate/distance", data=custom_object.get("data"))
    # print(response.json())


def return_distance(first_point, second_point):
    print("The difference between " + str(first_point) + "and " + str(second_point))
    loc1 = (first_point[0], first_point[1])
    loc2 = (second_point[0], second_point[1])
    hs.haversine(loc1, loc2)
    print(hs.haversine(loc1, loc2))
    return hs.haversine(loc1, loc2) * 1000


def calculate_total_distance():
    total = 0
    flag = 0
    rideRouteTracking = get_routes()
    points_lenght=len(rideRouteTracking)
    json_formatted_str = json.dumps(rideRouteTracking, indent=2)
    print(json_formatted_str)
    for i in range(0, len(rideRouteTracking) - 1):
        first_point = []
        second_point = []
        first_point.append(float(rideRouteTracking[i].get("latitude")))
        first_point.append(float(rideRouteTracking[i].get("longitude")))
        second_point.append(float(rideRouteTracking[i + 1].get("latitude")))
        second_point.append(float(rideRouteTracking[i + 1].get("longitude")))
        distance = return_distance(first_point, second_point)
        print(str(distance) + " " + "meters")
        total = distance + total
        if distance == 0:
            flag = flag + 1
        if distance > 150:
            print(
                "Bad Point ************************************************************************************************************************************************")
    print(flag)
    print("Total distance in kilometers")
    print(total / 1000)
    print("Total Points="+str(len(rideRouteTracking)))

    # total=total+distance
    # print(total)

    # "longitude": "72.9862292"
    # "latitude": "33.6845304"


def show_all_points_time():
    rideRouteTracking = sorted_list("driverCurrentTime")
    # sorted_routes=midrive_object
    for i in range(0, len(rideRouteTracking) - 1):
        time = giveme_time(rideRouteTracking[i].get("driverCurrentTime"))
        a = give_difference(rideRouteTracking[i].get("driverCurrentTime"),rideRouteTracking[i + 1].get("driverCurrentTime"))
        print("Time in minutes=" + " " + str(a / 60))


def show_sorted_sequence_numbers():
    rideRouteTracking = sorted_list()
    # sorted_routes = sorted(sorted_routes, key=lambda x: x["sequenceNumber"], reverse=False)
    for i in rideRouteTracking:
        i.get("sequenceNumber")


def print_sequence():
    temp_list = sorted_list("driverCurrentTime")
    print(temp_list)
    # pdb.set_trace()
    for i in temp_list:
        print(i.get("sequenceNumber"))
        print(giveme_time(i.get("driverCurrentTime")))


# def showpoints():
#     rideRouteTracking= sorted_list("sequenceNumber")
#     for i in rideRouteTracking:
#         print(i.get("latitude") + "," + i.get("longitude"))
#         print(giveme_time(i.get("driverCurrentTime"))
#
# def show_server_time():
#     rideRouteTracking=sorted_list("driverCurrentTime")
#     for i in rideRouteTracking:
#         print(giveme_time(i.get("receivingTime")))



if __name__ == '__main__':
    calculate_total_distance()
    # print(points_lenght)

