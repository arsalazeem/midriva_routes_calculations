import baseurl
import json
import requests
def login():
    session = requests.Session()
    login = {
        "email": "admin@midriva.com",
        "password": "admin1234"
    }
    response = session.post(baseurl.baseurl + "admin/logIn", data=login)
    print(response.json())
    return session


def create_driver():
    create_route = "admin/driver/create"
    print("Please enter a phone number")
    phonenumber = input()
    payload = {"identityDocuments": {
            "licenseSideA": "https://midriva.s3.amazonaws.com/profileImages/1000X1000/1632484782679-cat-551554__340.jpg",
            "licenseSideB": "https://midriva.s3.amazonaws.com/profileImages/1000X1000/1632484790603-cat-551554__340.jpg",
            "policeSideA": "https://midriva.s3.amazonaws.com/profileImages/1000X1000/1632484814826-arsal%20azeem%20image.jpg",
            "policeSideB": "https://midriva.s3.amazonaws.com/profileImages/1000X1000/1632484820273-arsal%20azeem%20image.jpg",
            "identitySideA": "https://midriva.s3.amazonaws.com/profileImages/1000X1000/1632484847008-arsal%20azeem%20image.jpg",
            "identitySideB": "https://midriva.s3.amazonaws.com/profileImages/1000X1000/1632484853969-cat-551554__340.jpg"
        },
        "vehicleDocuments": {
            "insuranceSideA": "https://midriva.s3.amazonaws.com/profileImages/1000X1000/1632484794293-cat-551554__340.jpg",
            "insuranceSideB": "https://midriva.s3.amazonaws.com/profileImages/1000X1000/1632484799761-cat-551554__340.jpg",
            "plateSideA": "https://midriva.s3.amazonaws.com/profileImages/1000X1000/1632484804425-cat-551554__340.jpg",
            "plateSideB": "https://midriva.s3.amazonaws.com/profileImages/1000X1000/1632484808773-cat-551554__340.jpg",
            "registrationSideA": "https://midriva.s3.amazonaws.com/profileImages/1000X1000/1632484825403-arsal%20azeem%20image.jpg",
            "registrationSideB": "https://midriva.s3.amazonaws.com/profileImages/1000X1000/1632484829668-arsal%20azeem%20image.jpg",
            "vehicleSideA": "https://midriva.s3.amazonaws.com/profileImages/1000X1000/1632484860234-cat-551554__340.jpg",
            "vehicleSideB": "https://midriva.s3.amazonaws.com/profileImages/1000X1000/1632484865156-cat-551554__340.jpg"
        },
        "name": "Arsal Azeem",
        "phoneNumberPrefix": "+92",
        "phoneNumberWithoutPrefix": phonenumber,
        "streetAddress": "Vizteck Soltutions phase 4 bahria town near to kfc",
        "postalCode": "46000",
        "city": "Islamabad",
        "state": "Federal",
        "dateOfBirth": "2000-12-04T19:00:00.000Z",
        "country": "Pakistan",
        "vehicleDetail": {
            "vehicleTypeId": "602badbef8def30ab96366e5",
            "vehicleModel": 2000,
            "vehicleTypeName": "Bike",
            "seatingCapacityArr": [{
                "min": 1,
                "max": "-2",
                "isActivated": True,
                "_id": "602badbef8def30ab96366e6"
            }],
            "seatingCapacityModel": 0,
            "plateNumber": "RIQ 851",
            "vehicleName": "Suzuki",
            "vehicleColor": "Aqua",
            "vehicleMaker": "Suzuki",
            "vehicleHorsePower": "400CC",
            "seatingCapacityId": "602badbef8def30ab96366e6",
            "seatingCapacity": "1-2"
        },
        "licenseNumber": "3432564756873",
        "licenceImage":"sdfasfda",
        "bankId": "602badbef8def30ab96366de",
        "accountTitle": "ARSAL AZEEM",
        "accountNumber": "2342353263453",
        "profileImage": "https://midriva.s3.amazonaws.com/profileImages/1000X1000/1632484774081-cat-551554__340.jpg",
        "email": "sirnew@gmail.com",
        "phoneNumber": "3034234234",
        "bankName": "Habib Bank Limited",
        "dob": 975956400
    }
    session=login()
    response=session.post(baseurl.baseurl+create_route,data=payload)
    print(response.json())
    session.close()


create_driver()


